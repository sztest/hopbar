ColourHop's default color change system is to "punch" to cycle through colors, which are indicated by a single small dot at the bottom center of the screen.  This mod replaces it with a more traditional hotbar, which aims to fix some issues:

- Possible to jump to a specific color using number hotkeys.
- Use of the scroll wheel or separate keybinds for forward/backward cycling.
- Hotbar touch areas on mobile now work instead of having to long-press to "punch".
- Patterns in hotbar indicators may be easier for colorblind users.
- Available colors are always visible.