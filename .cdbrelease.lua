-- LUALOCALS < ---------------------------------------------------------
local math, tonumber
    = math, tonumber
local math_floor
    = math.floor
-- LUALOCALS > ---------------------------------------------------------

-- luacheck: push
-- luacheck: globals config readtext readbinary

readtext = readtext or function() end
readbinary = readbinary or function() end

local stamp = tonumber("$Format:%at$")
if not stamp then return end
stamp = math_floor((stamp - 1540612800) / 60)
stamp = ("00000000" .. stamp):sub(-8)

return {
	user = "Warr1024",
	pkg = "hopbar",
	type = "mod",
	dev_state = "ACTIVELY_DEVELOPED",
	version = stamp .. "-$Format:%h$",
	title = "HopBar",
	short_description = "Hot-Key Color Selection for ColourHop",
	tags = {"hud"},
	content_warnings = {},
	license = "MIT",
	media_license = "MIT",
	repo = "https://gitlab.com/sztest/hopbar",
	maintainers = {"Warr1024"},
	long_description = readtext('README.md'),
	screenshots = {readbinary('.cdb1.jpg'), readbinary('.cdb2.jpg')}
}

-- luacheck: pop
