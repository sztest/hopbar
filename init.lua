-- LUALOCALS < ---------------------------------------------------------
local ch_colours, ch_player_api, getmetatable, ipairs, math, minetest,
      rawset, string, table, tostring
    = ch_colours, ch_player_api, getmetatable, ipairs, math, minetest,
      rawset, string, table, tostring
local math_floor, string_gsub, table_concat
    = math.floor, string.gsub, table.concat
-- LUALOCALS > ---------------------------------------------------------

-- Disable cycling via punch.
minetest.override_item("", {on_use = function() end})

-- Disable default hotbar background change when
-- changing player model.
do
	local oldset = ch_player_api.set_colour
	function ch_player_api.set_colour(player, ...)
		local meta = getmetatable(player)
		local oldhot = meta.hud_set_hotbar_image
		local function helper(...)
			rawset(meta, "hud_set_hotbar_image", oldhot)
			return ...
		end
		rawset(meta, "hud_set_hotbar_image", function() end)
		return helper(oldset(player, ...))
	end
end

local function resize(n)
	return "^[resize:" .. n .. "x" .. n
end
local function txesc(s)
	return string_gsub(string_gsub(tostring(s), "%^", "\\^"), ":", "\\:")
end

local barres = 32
local pad = 1
local time = 0

local function playerstep(player, data)
	local meta = player:get_meta()

	if not data.hudsel then
		data.hudsel = true
		player:hud_set_hotbar_selected_image("hopbar_sel.png")
	end

	local col = meta:get_int("colour")
	local widx = player:get_wield_index()
	if widx ~= col then
		meta:set_int("colour", widx)
		col = widx
		ch_player_api.set_colour(player, ch_colours.colour_name(col))
	end

	local maxcol = meta:get_int("max_col")
	if maxcol < 1 then maxcol = 3 end
	if maxcol ~= data.maxcol then
		data.maxcol = maxcol
		player:hud_set_hotbar_itemcount(maxcol)

		local txr = {"[combine:", (maxcol * barres + pad * 2), "x", barres + pad * 2}
		for i = 1, maxcol do
			txr[#txr + 1] = ":"
			txr[#txr + 1] = (i - 1) * barres + pad
			txr[#txr + 1] = ","
			txr[#txr + 1] = pad + math_floor(barres / 2)
			txr[#txr + 1] = "="
			txr[#txr + 1] = txesc(ch_colours.colour_name(i)
				.. ".png^[noalpha" .. resize(barres)
				.. "^[mask:hopbar_mask.png"
				.. "^[opacity:160")
		end
		player:hud_set_hotbar_image(table_concat(txr))
	end
end

do
	local player_data = {}
	minetest.register_globalstep(function(dtime)
			time = time + dtime
			for _, player in ipairs(minetest.get_connected_players()) do
				local pname = player:get_player_name()
				local data = player_data[pname]
				if not data then
					data = {}
					player_data[pname] = data
				end
				playerstep(player, data)
			end
		end)
	minetest.register_on_leaveplayer(function(player)
			player_data[player:get_player_name()] = nil
		end)
end
